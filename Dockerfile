FROM python:3
LABEL version="1.0.0" description="Disponibilizando api para teste de conexao serviços" maintainer="Pedro Lucas Souza <pedro.souza@mandic.net.br>"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential mysql.connector
COPY . /app
EXPOSE 5000
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]
